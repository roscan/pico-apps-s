# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/pico-sdk/tools/pioasm"
  "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/pioasm"
  "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/examples/ws2812_rgb/pioasm"
  "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/examples/ws2812_rgb/pioasm/tmp"
  "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/examples/ws2812_rgb/pioasm/src/PioasmBuild-stamp"
  "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/examples/ws2812_rgb/pioasm/src"
  "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/examples/ws2812_rgb/pioasm/src/PioasmBuild-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/examples/ws2812_rgb/pioasm/src/PioasmBuild-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "C:/Users/Administrator.ADMINTR-QA7FV09/Documents/College/Microprocessors/pico-apps/examples/ws2812_rgb/pioasm/src/PioasmBuild-stamp${cfgdir}") # cfgdir has leading slash
endif()
